#!/usr/bin/env python3

import random
import argparse
import numpy as np


# dataset file
DATASET_FILE = "./LinearRegressionDataSet_2.txt"

# default learning rate
ALPHA = 0.01
# default min batch size
MIN_BATCH = 200
# default learning times
LEARNING_TIMES = 4500


def generate_min_batch(dataset, batch_size):
    """
    Generate Min Batch.
    """
    dataset_shape = dataset.shape
    if dataset_shape[0] < batch_size:
        batch_size = dataset_shape[0]

    row_indexes = [i for i in range(dataset_shape[0])]
    random.shuffle(row_indexes)
    random_row_indexes = row_indexes[:batch_size]
    hsplit_indice = dataset_shape[1] - 1
    random_dataset = (dataset[random_row_indexes, :hsplit_indice],
                      dataset[random_row_indexes, hsplit_indice:])
    return random_dataset


def hypothesis(input_x, θ):
    """
    Hypothesis function 假设函数

    hθ(x) = θ.T * x
    """
    res = np.dot(input_x, θ.T)
    return res


def deviation(input_x, output_y, θ):
    """
    Deviation function 代价/偏差函数
    """
    res = sum([pow(hypothesis(x, θ) - y, 2) for x, y in zip(input_x, output_y)])
    return res / (2 * input_x.shape[0])


def gradientDescent(dataset, batch_size, learning_rate, learning_times):
    print("------Start gradient descent------")
    process = 0

    hsplit_indice = (dataset.shape)[1] - 1
    θ = np.random.rand(1, hsplit_indice)
    # print(θ)  # [[ 0.92081042  0.78857252]]
    # print(θ.shape)  # (1, 2)

    _θ = θ.copy()
    while process < learning_times:
        batch_data = generate_min_batch(dataset, batch_size)
        input_x = batch_data[0]
        output_y = batch_data[1]

        for i in range(θ.shape[1]):
            derivative = np.dot(
                (np.array([hypothesis(row, θ) for row in input_x]) - output_y).T,
                input_x[:, i:i+1])
            avg_derivative = derivative / batch_size
            # θ = θ - αJ(θ)'
            _θ[0, i] = θ[0, i] - learning_rate * avg_derivative

        θ = _θ
        # calculate deviation
        batch_test_data = generate_min_batch(dataset, batch_size)
        current_deviation = deviation(batch_test_data[0], batch_test_data[1], θ)
        if (process % 10 == 0):
            print("step %d, deviation %2f" % (process, current_deviation))
            print("θ: %s" % (θ))
        process += 1

    print("θ value is: %s" % (θ))
    print("------End gradient descent------")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--batch', type=int, default=MIN_BATCH)
    parser.add_argument('--epoch', type=int, default=LEARNING_TIMES)
    parser.add_argument('--rate', type=float, default=ALPHA)

    args = parser.parse_args()
    dataset = np.loadtxt(DATASET_FILE, dtype=np.float64)
    gradientDescent(dataset, args.batch, args.rate, args.epoch)
